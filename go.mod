module gocalc

go 1.16

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/lib/pq v1.10.2
	github.com/prometheus/client_golang v1.11.0
)
